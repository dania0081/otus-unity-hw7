using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homework : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Animator animator2;
    
    void Start()
    {
        animator.SetTrigger("Attack");
        Invoke("DeathAnim", 5f);
    }

    void DeathAnim()
    {
        animator2.SetBool("Die", true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
